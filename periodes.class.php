<?php
/* 
  Copyright 2017 SamuelBF

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* Periodes: indique si une date donnée est dans un ensemble de 
   périodes définies préalablement */

// Signification des dates :
// --15: 	tous les mois à partir du 15
// -05-15:	tous les ans à partir du 15 mai
// -05-:	tous les ans à partir du 1er mai
// 2017-05-15:	à partir du 15 mai 2017
// 2017-05-:	à partir du 1er mai 2017
// 2017--:	à partir du 1er janvier 2017
// 2017--15:	à partir du 15 janvier 2017
// :--20	tous les mois jusqu'au 20
// :-08-20	tous les ans jusqu'au 20 août
// :-08-	tous les ans jusqu'au 31 août
// :2017-08-20	jusqu'au 20 août 2017
// :2017-08-	jusqu'au 31 août 2017
// :2017--	jusqu'au 31 décembre 2017
// :2017--20	jusqu'au 20 décembre 2017
// Le principe:
// - un trou avant un ou des chiffre(s) signifie "toutes les valeurs"
//   (i.e. ça reste un trou, et si on en rencontre on continue la 
//   comparaison à l'étape d'après pour savoir si on est dans ou hors 
//   de l'intervalle)
// - un trou après un ou des chiffre(s) signifie "la valeur extrêmale"
//   (i.e. si on la rencontre, on la compare à la valeur courante et
//   on ne continue la comparaison que si la valeur qu'on regarde est 
//   égale à cette valeur extrêmale)
// Mise en forme canonique : 
// format	début		fin
// ---		---		---
// --X 		--X		--X
// -X- 		-X-01		-X-31
// -X-Y		-X-Y		-X-Y
// X--		-X-01-01	X-12-31
// X-Y-		X-Y-01		X-Y-31
// X--Z		X-01-Z		X-12-Z
// X-Y-Z	X-Y-Z		X-Y-Z

class Periodes{
  private $periodes, $bornes;

  public function __construct($str) {
    // Le format de la chaîne est décrit dans sondages.ini
    
    // On crée un tableau de paires début,fin
    $periodes = array_map(function($x) { return explode(':', $x); }, explode(',', $str));
    for($i=0; $i < count($periodes); $i++) {
      if(empty($periodes[$i][0]))
        $periodes[$i][0] = '--';
      if(empty($periodes[$i][1]))
        $periodes[$i][1] = '--';
    }
    
    // On le met sous forme canonique
    for($i=0; $i < count($periodes); $i++) {
      $debut = explode('-', $periodes[$i][0]);
      $fin = explode('-', $periodes[$i][1]);
      if(!empty($debut[0]) and empty($debut[1]))
      	$debut[1] = '01';
      if((!empty($debut[0]) or !empty($debut[1])) and empty($debut[2]))
        $debut[2] = '01';
      if(!empty($fin[0]) and empty($fin[1]))
      	$fin[1] = '12';
      if((!empty($fin[0]) or !empty($fin[1])) and empty($fin[2]))
        $fin[2] = '31';
      
      if(!empty($fin[0]) and $debut[0] > $fin[0]) {
        // Année de début de période > Année de fin: ça n'est pas censé arriver!
        $debut[0] = $fin[0];
      // On traite le cas où début > fin (-> ça ajoute une période)
      } else if(empty($debut[0]) and empty($fin[0])) {
        if(!empty($debut[1]) and !empty($fin[1]) and $debut[1] > $fin[1]) {
          $periodes[] = array('-01-'.(!empty($fin[2])?'01':''), implode('-', $fin));
          $fin = array('', '12', (!empty($debut[2])?'31':''));
        } else if($debut[1] == $fin[1]) {
          if(!empty($debut[2]) and !empty($fin[2]) and $debut[2] > $fin[2]) {
            $periodes[] = array('-'.(!empty($fin[1])?'01':'').'-01', implode('-', $fin));
            $fin = array('', (!empty($debut[1])?'12':''), '31');
          }
        }
      }
      $periodes[$i] = array($this->pad($debut[0]).'-'.$this->pad($debut[1]).'-'.$this->pad($debut[2]),
        $this->pad($fin[0]).'-'.$this->pad($fin[1]).'-'.$this->pad($fin[2]));

    }
    $this->bornes = array_shift($periodes);
    $this->periodes = $periodes;
  }
  
  /* Pad : on laisse la chaîne vide si elle est vide, on l'affiche sur au moins 2 caractères 
    sinon.
  */
  private function pad($str, $n=2) {
    return (empty($str)?'':sprintf('%0'.$n.'d', $str));
  }

  /* vers_chaine : retourne la chaîne canonique associée à cette définition de périodes.
    Cette chaîne est toujours la même pour une définition donnée, et elle est une entrée
    valide pour __construct.
  */
  public function vers_chaine() {
    sort($this->periodes);
    return implode(',', array_map(function($x) { return implode(':', $x); }, array_merge(array($this->bornes), $this->periodes)));
  }
  
  /* inferieur_ou_egal : indique si date a <= date b (au format [$d; $m;
  $j]). Les bornes sont incluses. On compare d'abord $d puis, si pas
  d'inégalité stricte ou si une des deux valeurs est nulle, $m puis ... */
  private function inferieur_ou_egal($a, $b) {
    // Si on arrive à un tableau vide : on renvoie vrai
    if(!count($a) or !count($b))
      return true;
    $v1 = array_pop($a);
    $v2 = array_pop($b);
    // Si une valeur est vide ou elles sont égales, on regarde plus loin
    if(empty($v1) or empty($v2) or $v1 == $v2)
      return $this->inferieur_ou_egal($a, $b);
    // Sinon, on se contente de comparer les valeurs
    return $v1 < $v2;
  }
  
  /* est_dans_periode : indique si une date est entre 2 bornes
     nos bornes sont incomplètes (de la forme '-10-') -> les valeurs 
     manquantes sont + ou moins l'infinie.
     Les bornes sont incluses dans la période voulue.
     $dt : date au format Y-m-d
     $bornes : array de str ('2017-05-16', '--4', ...).
   */
  private function est_dans_periode($date, $bornes) {
    $datea = array_reverse(explode('-', $date));
    $debut = array_reverse(explode('-', $bornes[0]));
    $fin = array_reverse(explode('-', $bornes[1]));
    return($this->inferieur_ou_egal($debut, $datea) and $this->inferieur_ou_egal($datea, $fin));
  }
  
  /* est_valide : renvoie un booléen indiquant si la date voulue est dans les périodes
     ou non.
  */
  public function est_valide($date) {

    // Première période: les bornes temporelles de validité
    if(!$this->est_dans_periode($date, $this->bornes))
      return FALSE;
      
    // Si on a pas d'autres limites : c'est bon
    if(!count($this->periodes))
      return TRUE;
      
    foreach($this->periodes as $periode) {
      if($this->est_dans_periode($date, $periode)) {
        return TRUE;
      }
    }
    
    // Si on a trouvé aucune période de validité : c'est pas bon
    return FALSE;
  }

  /* liste_jours : retourne la liste des jours valides pour les périodes en cours entre deux dates.
  */  
  public function liste_jours($dateDebut = 'now', $dateFin = 'now') {
    $dates = array();
    for($date = new DateTime($dateDebut), $fin = new DateTime($dateFin), $interv = new DateInterval('P1D'); $date <= $fin; $date->add($interv)) {
      $dateFmt = $date->format('Y-m-d');
      if($this->est_valide($dateFmt))
        $dates[] = $dateFmt;
    }
    return $dates;
  }

}

?>
