<?php
/* 
  Copyright 2016 SamuelBF

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* eAppointment : moteur de prise de RDV pour la Meurthe-et-Moselle et pour 
     Paris (cartes grises). Manifestement développé par la société esii 
     <https://www.esii.com/fr/rendez-vous>.

   Paramètres :
   - nom du site
   - motif (numéro)
   - motif2 (autre numéro, '1' par défaut)

   Si le premier motif n'est pas indiqué, on saute la première étape (sélection
   du premier motif).

   eAppointment (tm) nous renvoie du code javascript, relativement facile à
   interpréter pour ce qui nous intéresse.
*/

include_once 'configuration.php';
include_once 'log.class.php';
include_once 'sondage.class.php';

class Eappointment extends Sondage {

  protected $paramètres;

  function __construct($log, $dossier, $paramètres) {

    # Valeurs par défaut :
    $paramètres['cookies'] = isset($paramètres['cookies']) ? $paramètres['cookies'] : TRUE;
    $paramètres['proxy'] = isset($paramètres['proxy_eappointment']) ? $paramètres['proxy_eappointment'] : $paramètres['proxy'];
    if(!parent::__construct($log, $dossier, $paramètres)) {
      return;
    }
    $this->résultat = [null,null];
    $this->paramètres = $paramètres;

    $champs_communs = 'callCount=1&scriptSessionId=&batchId=0';

    if(isset($this->paramètres['motif'])) {
      # Ouverture la connexion : création du fichier de cookies
      $url = $this->paramètres["baseurl"]."/appointment.do?modify=false";
      if($this->charger_page($url, 'a', 'raw') === false) {
        $this->log(Log::ERREUR, "On arrive pas à se connecter au site internet, on arrête.");
        $this->abandon();
        return;
      }


      # On indique le motif (premier motif) :
      curl_setopt($this->connexion, CURLOPT_POST, TRUE);
      $url = $this->paramètres['baseurl'].'/dwr/call/plaincall/AjaxMotive.motiveMultiSiteSubmit.dwr';
      curl_setopt($this->connexion, CURLOPT_POSTFIELDS, $champs_communs.
      '&c0-id=0&c0-scriptName=AjaxMotive&c0-methodName=motiveMultiSiteSubmit&c0-param0=boolean:false&c0-param1=Object_Object:{'.$this->paramètres['motif'].':reference:c0-e1}&c0-e1=string:1');

      $JSRésultat = $this->charger_page($url, 'b', 'raw');
      if(!$this->verifier($JSRésultat)) return;
    }

    if(!isset($paramètres['motif2'])) {
      # On tente de trouver le second motif
      $url = $this->paramètres['baseurl'].'/dwr/call/plaincall/AjaxSelectionFormFeeder.getServiceBeanList.dwr';
      curl_setopt($this->connexion, CURLOPT_POSTFIELDS, $champs_communs.
      '&c0-id=0&c0-scriptName=AjaxSelectionFormFeeder&c0-methodName=getServiceBeanList&c0-param0=boolean:false&c0-param1=string:'.$this->paramètres['site']);
      $JSRésultat = $this->charger_page($url, 'b2', 'raw');
      if(!$this->verifier($JSRésultat)) return;

      $services = $this->extraire_motifs2(str_replace('"', '', $JSRésultat));
      # Cas où on a pas de motifs :
      if(count($services) == 0) {
        $this->log(Log::ERREUR, "On a trouvé aucun motif possible en b2. On arrête.");
        $this->abandon();
        return;
      }

      # Cas où on a plusieurs motifs :
      if(count($services) > 1) {
        foreach($services as $service) {
          $liste .= "{$service['serviceKey']} => {$service['name']}, ";
        }
        $this->log(Log::ERREUR, "On a trouvé plusieurs motifs possible en b2 : {$liste}. On arrête.");
        $this->abandon();
        return;
      }

      $this->paramètres['motif2'] = $services[0]['serviceKey'];
      $this->log(Log::DEBUG, "On a trouvé motif2 : {$this->paramètres['motif2']}.");
    }

    # On charge la liste des jours fermés (les jours ouverts s'en déduisent par différence) en indiquant
    # le site et le second motif.
    $url = $this->paramètres['baseurl'].'/dwr/call/plaincall/AjaxSelectionFormFeeder.getClosedDaysList.dwr';
    curl_setopt($this->connexion, CURLOPT_POSTFIELDS, $champs_communs.
    '&c0-scriptName=AjaxSelectionFormFeeder&c0-methodName=getClosedDaysList&c0-id=0&c0-param0=boolean:false&c0-param1=string:'.$this->paramètres['site'].'&c0-param2=string:'.$this->paramètres['motif2']);

    $JSRésultat = $this->charger_page($url, 'c', 'raw');
    if(!$this->verifier($JSRésultat)) return;
    $JSRésultat = str_replace('"', '', $JSRésultat);

    if(($posDeb = strpos($JSRésultat, '[')) < ($posFin = strpos($JSRésultat, ']'))) {
      # on extrait le tableau entre les parenthèses :
      $datesFermées = explode(',', substr($JSRésultat, $posDeb + 1, $posFin - $posDeb -1));
      $nbOuvert = 0;
      # On lit le tableau en comparant les dates $i-1 et $i :
      for($i = 1; $i < count($datesFermées) && $nbOuvert < 2; $i++) {
        $dateDébut = new DateTime($datesFermées[$i-1]);
        $dateFin = new DateTime($datesFermées[$i]);
        # Si on trouve des dates libres, on les ajoute :
        for($dateCourant = $dateDébut->add(new DateInterval('P1D')); $dateCourant < $dateFin && $nbOuvert < 2; $dateCourant = $dateCourant->add(new DateInterval('P1D'))) {
          $this->résultat[$nbOuvert] = $dateCourant->format('Y-m-d');
          $this->log(Log::INFORMATION, 'Date trouvée : '.$this->résultat[$nbOuvert]);
          $nbOuvert++;
        }
      }
    }
    # Fin !
    $this->fermeture_connexion();
  }

  // extraire_motifs2 : lit la réponse à getServiceBeanList et renvoie un tableau:
  // [id => ['serviceKey' => motif2, 'name' => nom]]
  private function extraire_motifs2($str) {
    $resultat = array();
    foreach(explode(';', $str) as $petitbout) {
      $decoupe = explode('=', $petitbout, 2);
      $decoupe2 = explode('.', $decoupe[0]);
      if(count($decoupe2) > 1 and ($decoupe2[1] == 'name' or $decoupe2[1] == 'serviceKey')) {
        $id = substr($decoupe2[0], 1);
        if(!isset($resultat[$id])) $resultat[$id] = array();
        $resultat[$id][$decoupe2[1]] = $decoupe[1];
      }
    }
    return $resultat;
  }

  // verifier : renvoie 'faux' si on rencontre une erreur.
  protected function verifier($contenu) {
    if(strpos($contenu,'Error') or strpos($contenu,'Exception') or $contenu === false) {
        $this->log(Log::ERREUR, "Il y a une erreur pour la dernière requête effectuée. On arrête");
        $this->abandon();
        return false;
    }
    return true;
  }
}

?>
