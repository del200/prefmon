#!/usr/bin/php -f
<?php
/* 
  Copyright 2016 SamuelBF

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/*  iniVersBDD.php : lit le fichier sondages.ini et met à jour la table Sondages en fonction */


include_once 'configuration.php';
include_once 'log.class.php';
include_once 'periodes.class.php';

$log = new Log(Log::AVERTISSEMENT, CHEMIN_ABSOLU.'/'.$configuration['logs'].'/'.date('Y-m-d').'.log', true);

$log->message(Log::DEBUG, 'Lecture du fichier '.CHEMIN_ABSOLU.'/sondages.ini');
if(!$sondages = parse_ini_file(CHEMIN_ABSOLU.'/sondages.ini', true)) {
  $log->message(Log::ERREUR, 'Impossible de lire le fichier '.CHEMIN_ABSOLU.'/sondages.ini. Abandon.');
  exit(255);
}


function erreur_sql($sql, $log) {
  $log->message(Log::ERREUR, "Erreur d'exécution de la requête SQL ($sql->errno) : $sql->error");
	$log->message(Log::ERREUR, "Mise à jour abandonnée et annulée");
	$sql->rollback();
	$sql->close();
	exit(1);
}

# Ajoute un lien vers le sondage à la description proposée, si possible
function description($paramètres) {
  $description = isset($paramètres['description']) ? $paramètres['description'] : '';

  # Lien 'prendre un rendez-vous' :
  if(!isset($paramètres['actif']) or $paramètres['actif']) {
    if(substr($paramètres['système'], 0, 12) == 'EZBooking_41' and isset($paramètres['baseid'])) { 
      $description .= ' / <a href="'.$paramètres['baseurl'].'/booking/create/' . $paramètres['baseid'];
      $description .= '">prendre un RDV</a>';
    } elseif($paramètres['système'] == 'EZBooking_41') {
      $description .= ' / <a href="'.$paramètres['baseurl'].'/rendez-vous';
      $description .= '">prendre un RDV</a>';
    } elseif($paramètres['système'] == 'Eappointment') {
      $description .= ' / <a href="'.$paramètres['baseurl'].'/appointment.do?modify=false';
      $description .= '">prendre un RDV</a>';
    }
  }
  
  if($paramètres['système'] == 'EZBooking_41')
    $description .= ' / <a href="'.$paramètres['baseurl'].'">site de la préfecture</a>';
  
  return $description;
}

# Lien avec la base de données :
$log->message(Log::DEBUG, "Ouverture de la connexion SQL (hôte ".$configuration['sql']['serveur'].")");
$sql = new mysqli($configuration['sql']['serveur'], $configuration['sql']['utilisateur'],
$configuration['sql']['motdepasse'], $configuration['sql']['basededonnées']);
# On désactive l'autocommit, ce qui permet d'annuler toute les modifications en cours si on rencontre une erreur.
$sql->autocommit(false);

if ($sql->connect_errno) {
  $log->message(Log::ERREUR, "Erreur de connexion SQL ($sql->connect_errno) : $sql->connect_error");
  exit(1);
}
$sql->query('SET NAMES UTF8;');

# On récupère la liste des sondages dans le fichier ini.
$ini = array();
foreach($sondages as $titre=>$paramètres) {
  # Héritage : [91/evry] hérite de [91], puis [91/evry/etudiant] hérite de [91/evry] ...
  if($fin = strrpos($titre, '/')) {
    $log->message(Log::DEBUG, 'La section '.$titre.' hérite des paramètres de la section '.substr($titre, 0, $fin));
    $paramètres = array_merge($sondages[substr($titre, 0, $fin)], $paramètres);
    $sondages[$titre] = $paramètres;
  }

  # On note le sondage si on a un IDProcédure :
  if(isset($paramètres['IDProcédure'])) {
    $nomDossier = substr($titre, 3);
    $nomProcedure = isset($paramètres['nom']) ? $paramètres['nom'] : $nomDossier;
    $exclusif = isset($paramètres['exclusif']) ? ($paramètres['exclusif'] ? 'true':'false') : 'false';
    $periodes = isset($paramètres['périodes']) ? (new Periodes($paramètres['périodes']))->vers_chaine() : '';
    $IDProc = intval('0x'.$paramètres['IDProcédure'],16);
    $dep = $IDProc >> 8;
    $ini[$IDProc] = array('Département'=>$dep,'NomDossier'=>$nomDossier,
      'NomProcédure'=>$nomProcedure,'Exclusif'=>$exclusif, 'Description'=>description($paramètres), 'Périodes'=>$periodes);
  }
}

# On récupère la liste des sondages dans la base de données
if(!($sqlListe = $sql->query('SELECT ID FROM Procédures;'))) erreur_sql($sql, $log);
  
$sqlID = array();
while($ligne=$sqlListe->fetch_row()) $sqlID[] = $ligne[0];

# On compare les 2 liste d'IDs
$iniID = array_keys($ini);
$dansIniPasDansSql = array_diff($iniID, $sqlID);
$dansSqlPasDansIni = array_diff($sqlID, $iniID);
$dansIniEtSql = array_intersect($iniID, $sqlID);

# On ajoute ceux qui manquent dans la base de données :
$reqInsertion = 'INSERT INTO Procédures(ID, Département, NomDossier, NomProcédure, Exclusif, Description, Périodes) VALUES ';
foreach($dansIniPasDansSql as $IDProcedure) {
  $parametres = $ini[$IDProcedure]; 
  $reqInsertion .= '('.$IDProcedure.', '.$parametres['Département'].', "'.$parametres['NomDossier'].'", "'.$parametres['NomProcédure'].
    '", '.$parametres['Exclusif'].', "'.$sql->escape_string($parametres['Description']).'", "'.
    $sql->escape_string($parametres['Périodes']).'"), ';
}
$reqInsertion = substr($reqInsertion, 0, -2).';';

if(!count($dansIniPasDansSql)) {
  $log->message(Log::INFORMATION, 'Pas de procédure manquant dans la base de données.');
} else {
  $log->message(Log::INFORMATION, 'Insertion des procédures manquantes dans la base de données : ' . implode(', ', $dansIniPasDansSql));
  
  if(!$sql->query($reqInsertion)) erreur_sql($sql, $log);
}

# On supprime ceux qui sont en trop dans la base de données :
if(!count($dansSqlPasDansIni)) {
  $log->message(Log::INFORMATION, 'Pas de procédure en trop dans la base de données.');
} else {
  $supprID = implode(', ', $dansSqlPasDansIni);
  $log->message(Log::INFORMATION, 'Suppression des procédures :'.$supprID);
  if(!$sql->query('DELETE FROM Procédures WHERE ID IN ('.$supprID.');')) erreur_sql($sql, $log);
}

# On met à jour tous les autres
if(!count($dansIniEtSql)) {
  $log->message(Log::INFORMATION, 'Pas de procédure à mettre à jour dans la base de données.');
} else {
  foreach($dansIniEtSql as $IDProcedure) {
    $parametres = $ini[$IDProcedure]; 
    $log->message(Log::INFORMATION, 'Mise à jour de la procédure #'.$IDProcedure);
    if(!$sql->query('UPDATE Procédures SET Département='.$parametres['Département'].', NomDossier="'.$parametres['NomDossier'].
      '", NomProcédure="'.$parametres['NomProcédure'].'", Description=\''.$sql->escape_string($parametres['Description']).
      '\', Périodes=\''.$sql->escape_string($parametres['Périodes']).'\' WHERE ID="'.$IDProcedure.'";')) erreur_sql($sql, $log);
  }
}

$sql->commit();
$log->message(Log::DEBUG, "Fermeture de la connexion SQL");
$sql->close();

?>
